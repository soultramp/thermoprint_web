<script type="text/javascript" src="http://ajax.googleapis.com/ajax/
libs/jquery/1.3.0/jquery.min.js"></script>

<script>
	function popUp() {
		var popup = document.getElementById("myPopup");
		popup.classList.toggle("show");
	}

	window.onload = setupRefresh;
	function setupRefresh()
	{
	    setInterval(refreshBlock,3000);
	}

	function refreshBlock()
	{
	    $('#update').load("voltage.php");
	}

</script>
<!DOCTYPE HTML>
<html>
<head>
	<title>
        	Thermoprint Web Interface
	</title>

	<link rel="icon" href="https://kombinat-lump.de/wp-content/uploads/2019/10/cropped-logo_final-32x32.png" sizes="32x32" />

	<link href="css/style.css" rel="stylesheet">

	<style>
		body { background-image: url('https://kombinat-lump.de/wp-content/uploads/2020/06/website_hintergrund_neu.jpg');
			background-repeat: no-repeat;
			background-size: cover;}
	</style>
</head>

<body>



<div>
	<div><img src="https://kombinat-lump.de/wp-content/uploads/2020/04/logo_final_weiss-300x229.png" alt="LumpLogo" class="center-pic"></div>
	<div class="center">


		<div class="power" id="update">
			<?php include("voltage.php") ?>
		</div>

			<div class="title">
				<b>Drop file to upload</b>
				<div class="popup" onclick="popUp()"><img src="img/help.png" alt="help" width="20" height="20">
					<span class="popuptext" id="myPopup" style="font-size:14">
					Upload:</br>
					<hr>
					Create some pdf files with the content you want to print.
					The paper width is only 58mm, so the pdf document must have a matching format (NOT A4).
					Compress the files in a zip container and upload it (click on the cloud icon, choose your zip file, click the upload button).</br>
					<span style="color:red">All previously uploaded files will be deleted!</br></span>
					</br>Update:</br>
					<hr>
					By clicking the "Update Firmware" button the printer will download and install the latest version of the printing firmware and web interface.
					Therefore the printer has to be connected to a wifi with a working internet connection.
					</span>
				</div>
			</div>
			<form action="#" method="post" enctype="multipart/form-data">
			<div class="dropzone">
				<img src="http://100dayscss.com/codepen/upload.svg" class="upload-icon" />
				<input type="file" name="file" size="50? maxlength="100000? accept=".zip" class="upload-input"/> 
			</div>
			<button type="submit" class="btn" name="SubmitButton">Upload File</button>
	        	</form>
			<div>
				<form action="#" method="post" enctype="multipart/form-data">
				<input type="submit" name="UpdateButton" class="btn" value="Update Firmware" />
                                </form>
			</div>
		</div>
	</div>

	<div class="log" style="font-family:monospace">

	<?php
	    if(isset($_POST['SubmitButton'])){
	        error_reporting(E_ALL);
	            ini_set("display_errors", 1);
	            if ($_FILES["file"]["error"] > 0)
        	    {
	                $message = "nofile";
	            }
	            else
	            {
			// check for existing files and delete them
			$files = glob('/var/www/uploads/*');
			foreach($files as $file){
			if(is_file($file))
			    unlink($file);
			}

			$filename = pathinfo($_FILES["file"]["name"]);
			if ($filename['extension'] == 'zip')
			{
				move_uploaded_file($_FILES["file"]["tmp_name"], "/var/www/uploads/". $_FILES["file"]["name"]);
				unzip();
				$message = "success";
			}
			else
			{
				$message = "wrongfiletype";
			}
	            }
	    }

	    else if(isset($_POST['UpdateButton'])){
		echo  nl2br ("Updating firmware:\r\n");
		system("/bin/bash /home/pi/thermoprint/git-pull.sh");
		echo  nl2br ("\r\n-------------------\r\n");
		echo  nl2br ("Updating interface:\n");
	        system("/bin/bash /var/www/scripts/git-pull.sh");
		echo  nl2br ("\r\n");
		$message = "update";
	    }

	    function unzip(){
		if (!is_dir('/var/www/uploads/unzipped')){
			mkdir ( '/var/www/uploads/unzipped');
		}
		// check for existing files and delete them
		$files = glob('/var/www/uploads/unzipped/*');
		foreach($files as $file){
		if(is_file($file))
		    unlink($file);
		}
		$file = "/var/www/uploads/" . $_FILES["file"]["name"];
		$systemUnzipOrder = "unzip " . $file . " -d /var/www/uploads/unzipped/";
		exec($systemUnzipOrder);
	    }
	?>

	</div>

	<?php if ($message == "success"): ?>
		<div class='message'>
                <b>Upload and processing successful!</br>Restarting services...</b>
		<?php exec("/bin/bash /home/pi/thermoprint/restart_thermoprint.sh");?>
		<b>...done!</b>
		</div>

	<?php elseif ($message == "nofile"): ?>
		<div class='message'>
                <b>Please select a file </br> and try again.</b>
		</div>
	<?php elseif ($message == "wrongfiletype"): ?>
		<div class='message'>
                <b>Wrong file extension. </br> Please upload a .zip file.</b>
		</div>
	<?php elseif ($message == "update"): ?>
		<div class='message'>
                <b>Firmware update done.</b>
		</div>
	<?php endif; ?>

	<div class="stat_button"><a href="stats.php"><img src="img/bar-chart.png" alt="stats" style="width:40px"></a></div>

    </div>
    </body>
</html>

