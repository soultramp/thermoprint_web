<!DOCTYPE HTML>
<html>
<head>
<title>
	Thermoprint Web Interface
</title>
<link href="css/style.css" rel="stylesheet">
<link rel="icon" href="https://kombinat-lump.de/wp-content/uploads/2019/10/cropped-logo_final-32x32.png" sizes="32x32" />

<style>
	body { background-image: url('https://kombinat-lump.de/wp-content/uploads/2020/06/website_hintergrund_neu.jpg');
		background-repeat: no-repeat;
		background-size: cover;}
</style>

<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/canvasjs.min.js"></script>

<script type="text/javascript">

window.onload = function () {

    var dataPoints = [];
    var chart = new CanvasJS.Chart("chartContainer",{
      theme: "light2",
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
      title:{
             text: "Thermoprinter Statistics",
              fontSize: 30
      },
      subtitles:[{
             text: "Select area to zoom.",
      }],
      axisY: {
            title: "Amount of Prints",
      },
      data: [{
          type: "column",
          dataPoints: dataPoints
      }]
    });


    $.get("http://thermoprinter.local/stats.csv", getDataPointsFromCSV);

    function getDataPointsFromCSV(csv) {

        var csvLines = [];
        csvLines = csv.split(/[\r?\n|\r|\n]+/);
        for (var i = 0; i < csvLines.length-1; i++) {
            var points = 1;
            while(csvLines[i] == csvLines[i+1]) {
                i++;
                points++;
            }

            dataPoints.push({
                label: csvLines[i],
                y: Number(points)
            });
        }
        chart.render();

	document.getElementById("counter_total").innerHTML = csvLines.length;
    }
}
</script>
</head>
<body>
	<div class="stat_box">
		<div id="chartContainer" style="height: 450px; width: 90%;"></div>
	</div>


<div class="stat_button">
	<a href="index.php"><img src="img/back.png" alt="back" style="width:40px"></a>
</div>

<div class="counter_area">
	<div class="counter_box">
		<div class="counter_big" id="counter_total"></div>
		<div class="counter_small">TOTAL PRINTS</div>
	</div>
</div>
</body>
</html>
